FROM debian

RUN apt-get update
RUN apt-get install -y \
	git \
	g++ \
	cmake \
	make \
	wget \
	python3 \
	build-essential \
	libgl1-mesa-dev \
	libglu1-mesa-dev \
	libclang-dev \
	python-dev

# Point Cloud Library
RUN apt-get install -y libpcl-dev

# Qt 5
RUN apt-get install -y qtbase5-dev

# SFML 
RUN apt-get install -y libsfml-dev

# Boost 
RUN apt-get install -y libboost-all-dev

# Poco
RUN apt-get install -y libpoco-dev

# OpenCV
RUN apt-get install -y libopencv-dev

# DLib
RUN apt-get install -y libdlib-dev

# Eigen
RUN apt-get install -y libeigen3-dev