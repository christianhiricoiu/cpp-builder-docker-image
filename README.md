# Purpose 

I built this docker image to compile all my c++ programs, having the dependencies I need all in one place.

# Requirements

- Docker

# How to get started
## 1. Build the docker image
- clone the repository and open a terminal in it
- run : `docker build -t debian_cpp_builder .`
- Finally, create a new container : `docker run --name Builder_contain debian_cpp_builder`
## 2. Compile your programs with cmake
Now that your container is up and running, you can use it to compile your c++ projects configured with cmake:
- in a terminal, open the container : `docker exec -it Builder_contain /bin/bash`
- you are now in the container : `cd /home/` 
- clone the repo of your c++ program, e.g. : `git clone https://gitlab.com/christianhiricoiu/fluidpp && cd fluidpp`
- create a build directory : `mkdir build`
- compile your program : `/usr/bin/cmake --build ./build/ --config Debug --target all -j 8`
- if compilation goes well, you will find your executable in the build directory

## 3. Extract your program from docker container
- open a terminal and copy the file : `docker cp Builder_contain:/home/fluidpp/build/fluidpp ./local-dir`
- run it : `./local-dir/fluidpp` 
